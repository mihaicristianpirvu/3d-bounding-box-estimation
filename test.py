import numpy as np
import matplotlib.pyplot as plt

from test_dataset import draw3DBBox
from loss_functions import get2Dto3DPoint

def compute3DCenters(centerOffsets, bbox2dCenters, camera, Z):
	MB = len(centerOffsets)
	results = np.zeros((MB, 3), dtype=np.float32)
	# Project each 2d center to 3D using the camera matrix and the given Z (distance)
	for i in range(MB):
		results[i] = get2Dto3DPoint(bbox2dCenters[i], camera, Z[i])
	# Add the computed offsets for each 3D projected point to get the center of the 3D bbox (as predicted by model)
	results[:, 0 : 2] += centerOffsets
	return results

def plotThisImage(rgb, results, labels, imageIndex):
	thisLabelsIx = np.where(labels["originalRGBIndex"] == imageIndex)[0]
	labels = {k : labels[k][thisLabelsIx] for k in ["3dDimensions", "3dRotationY", "camera", \
		"3dCenter", "bbox2dCenter", "Z"]}
	results = {k : results[k][thisLabelsIx] for k in ["3dDimensions", "3dRotationY", "3dCenterOffset"]}

	camera = labels["camera"][0]
	results["3dCenter"] = compute3DCenters(results["3dCenterOffset"], labels["bbox2dCenter"], camera, labels["Z"])

	labelCenters, labelDims, labelAlphas = labels["3dCenter"], labels["3dDimensions"], labels["3dRotationY"]
	resCenters, resDims, resAlphas = results["3dCenter"], results["3dDimensions"], results["3dRotationY"]
	numLabels = len(labelCenters)

	for i in range(numLabels):
		center, dim, alpha, labelAlpha = resCenters[i], resDims[i], resAlphas[i], labelAlphas[i]
		# Find offset between real and predicted alpha. If it's below 30 degrees, color the box green.
		diff = (np.abs(alpha - labelAlphas[i]) % (2 * np.pi)) / (2 * np.pi) * 360
		color = (1, 0, 0) if diff > 30 else (0, 1, 0)
		rgb = draw3DBBox(rgb, center, dim, alpha, camera, color=color)
	plt.imshow(rgb)
	plt.show()

def test(model, generator, numIters):
	for (data, labels) in generator:
		results = model.npForward(data)
		rgbs = labels["originalRGB"]
		numOrigImages = len(rgbs)

		for i in range(numOrigImages):
			plotThisImage(rgbs[i], results, labels, imageIndex=i)