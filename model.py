import torch as tr
import torch.nn as nn
import torch.nn.functional as F
from torchvision import models
from neural_wrappers.pytorch import NeuralNetworkPyTorch

class Model(NeuralNetworkPyTorch):
	def __init__(self):
		super().__init__()

		self.baseModel = models.vgg16(pretrained=True)
		for param in self.baseModel.parameters():
			param.requires_grad = False
		self.baseModel.classifier[6] = nn.Linear(4096, 100)
		self.fc1Position = nn.Linear(100, 100)
		self.fc2Position = nn.Linear(100, 2)
		self.fc1Dimension = nn.Linear(100, 100)
		self.fc2Dimension = nn.Linear(100, 3)
		self.fc1Rotation = nn.Linear(100, 100)
		self.fc2Rotation = nn.Linear(100, 2)

	def forward(self, x):
		x = x.transpose(1, 3).transpose(2, 3)
		y1 = F.relu(self.baseModel(x))

		yPosition = F.relu(self.fc1Position(y1))
		yPosition = self.fc2Position(yPosition)

		yDimension = F.relu(self.fc1Dimension(y1))
		yDimension = self.fc2Dimension(yDimension)

		yRotation = F.relu(self.fc1Rotation(y1))
		yRotation = self.fc2Rotation(yRotation)

		result = {"3dCenterOffset" : yPosition, "3dDimensions" : yDimension, "3dRotationYTrig" : yRotation}

		# L2 normalize the angle values
		result["3dRotationYTrig"] = result["3dRotationYTrig"].abs()
		norm = (result["3dRotationYTrig"][:, 0]**2 + result["3dRotationYTrig"][:, 1]**2).sqrt().unsqueeze(-1)
		result["3dRotationYTrig"] = result["3dRotationYTrig"] / norm
		# Store actual angle, so we make the regression using it.
		result["3dRotationY"] = tr.atan2(result["3dRotationYTrig"][:, 0], result["3dRotationYTrig"][:, 1])

		return result