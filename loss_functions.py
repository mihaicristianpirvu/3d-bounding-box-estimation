import numpy as np
import torch as tr

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def toHom(x):
	return np.array([*x, 1], dtype=x.dtype)

def fromHom(x):
	return x[0 : -1] / x[-1]

# Z is ... depth to object center. Not sure if we need this or we can work in the 3D space with Z = 1
def get2Dto3DPoint(point2D, camera, Z):
	inverseCamera = np.copy(camera)
	inverseCamera[0 : 3, 0 : 3] = np.linalg.inv(camera[0 : 3, 0 : 3])
	inverseCamera[0 : 3, -1] = -camera[0 : 3, -1]
	point2D = toHom(point2D) * Z
	center2Dto3D = inverseCamera[0 : 3, 0 : 3] @ (point2D + inverseCamera[0 : 3, -1])
	return center2Dto3D

# https://cs.gmu.edu/~amousavi/papers/3D-Deepbox-Supplementary.pdf
# We can minimize -cos(y - t), where y and t are alpha angles directly. This is same as minimizing squared loss of
#  angles: L = (cos(t) - cos(y))**2 + (sin(t) - sin(y))**2 =
#   = [cos(t)**2 + cos(y)**2 - 2cos(y)cos(t)] + [sin(t)**2 + sin(y)**2 - sin(y)sin(t)] =
#   = 1 + 1 - 2 (cos(y)cos(t) + sin(y)sin(t)) = 2 - 2 * cos(y - t) ~= -cos(y - t)
# Final improvements: L = ( 1 - cos(y-t) ) / 2, to get a loss in [0, 1].
def angleLossFn(y, t, **kwargs):
	return ((1 - tr.cos(y["3dRotationY"] - t["3dRotationY"])) / 2).mean()

def angleMetricFn(y, t, **kwargs):
	return ((1 - np.cos(y["3dRotationY"] - t["3dRotationY"])) / 2).mean()

def angleDegreeMetricFn(y, t, **kwargs):
	diff = np.abs(y["3dRotationY"] - t["3dRotationY"])
	diff %= 2 * np.pi
	diff /= 2 * np.pi
	diff *= 360
	diff = diff.mean()
	return diff

# Project 2D center to 3D space, using the camera and a given Z (distance). By default we use the real distance to
#  the object (t["Z"]), from the actual 3D bbox center. Then, the prediction is the offset (on X and Y, since we
#  equalize Z to the desired distance).
def centerLossFn(y, t, **kwargs):
	MB = t["bbox2d"].shape[0]

	# Project 2D center to 3D space, according to the Z value, as given by t["Z"]. By default, this is the 3rd
	#  dimension of 3dCenter (distance to object). But, we can work in any space (such as Z=1) or Z=predicted depth.
	bbox2dCenterTo3D = tr.zeros(MB, 3).to(device)
	tNP = {k : t[k].cpu().numpy() for k in ["bbox2dCenter", "camera", "Z"]}
	for i in range(MB):
		res = get2Dto3DPoint(tNP["bbox2dCenter"][i], tNP["camera"][i], tNP["Z"][i])
		bbox2dCenterTo3D[i] = tr.FloatTensor(res).to(device)

	# Update the distances to the labels according to the desired Z (which is the actual distance anyway for now,
	#  so nothing changes, but if I ever add a depth module to it, it will change, or if we just compare in Z=1 space)
	t["3dCenter"] *= (t["Z"] / t["3dCenter"][:, -1]).unsqueeze(-1)

	centersOffset = t["3dCenter"][:, 0 : -1] - bbox2dCenterTo3D[:, 0 : -1]
	return ((y["3dCenterOffset"] - centersOffset)**2).mean()

# Project the 3D centers to 2D space, and find the difference between true offset (3D - 2D centers) against the
#  predicted offset.
def centerMetricFn(y, t, **kwargs):
	MB = t["bbox2d"].shape[0]
	projected3DCenters = np.zeros((MB, 2), dtype=np.float32)

	for i in range(MB):
		projected3DCenters[i] = fromHom(t["camera"][i] @ toHom(t["3dCenter"][i]))

	centersOffset2D = projected3DCenters - t["bbox2dCenter"]
	return np.abs(y["3dCenterOffset"] - centersOffset2D).mean()

def dimensionsLossFn(y, t, **kwargs):
	return ((y["3dDimensions"] - t["3dDimensions"])**2).mean()

def lossFn(y, t):
	angleLoss = angleLossFn(y, t)
	centerLoss = centerLossFn(y, t)
	dimensionsLoss = dimensionsLossFn(y, t)
	return angleLoss + centerLoss + dimensionsLoss