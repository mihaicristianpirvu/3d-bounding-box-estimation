import matplotlib.pyplot as plt
import numpy as np
import cv2

def draw2DBBox(rgb, bbox2d, bbox2dCenter, color):
	j_min, i_min, j_max, i_max = bbox2d
	topLeft, topRight, bottomRight, bottomLeft = (j_min, i_min), (j_max, i_min), (j_max, i_max), (j_min, i_max)

	cv2.line(rgb, topLeft, topRight, color=color)
	cv2.line(rgb, topRight, bottomRight, color=color)
	cv2.line(rgb, bottomRight, bottomLeft, color=color)
	cv2.line(rgb, bottomLeft, topLeft, color=color)
	cv2.circle(rgb, tuple(bbox2dCenter), radius=3, color=color)
	return rgb

def draw3DBBox(rgb, center, dim, angleRotY, camera, color):
	def toHom(x):
		return np.array([*x, 1], dtype=x.dtype)
	def fromHom(x):
		return x[0 : -1] / x[-1]

	# For some reason, the center of the object has no height. So the center of the 3D bounding box must be moved
	#  height / 2 above it's position.
	movedCenter = np.array([center[0], center[1] - dim[0] / 2, center[2]], dtype=np.float32)
	projCenter = fromHom(camera @ toHom(movedCenter))

	cv2.circle(rgb, tuple(projCenter), radius=3, color=color)

	# Dear Internet, thanks for helping me again... Still don't know HTF the projection is done
	corners = []
	for i in [1, -1]:
		for j in [1, -1]:
			for k in [0, 1]:
				point = np.copy(center)
				point[0] += i * dim[1] / 2 * np.cos(-angleRotY + np.pi / 2) + (j*i) * dim[2] / 2 * np.cos(-angleRotY)
				point[1] += -k * dim[0]
				point[2] += i * dim[1] / 2 * np.sin(-angleRotY + np.pi / 2) + (j*i) * dim[2] / 2 * np.sin(-angleRotY)
				point = np.int32(fromHom(camera @ toHom(point)))
				corners.append(tuple(point))

	# Draw the 14 lines
	# 4 top lines
	cv2.line(rgb, corners[1], corners[7], color=color)
	cv2.line(rgb, corners[7], corners[5], color=color)
	cv2.line(rgb, corners[5], corners[3], color=color)
	cv2.line(rgb, corners[3], corners[1], color=color)
	# 4 bottom lines
	cv2.line(rgb, corners[0], corners[6], color=color)
	cv2.line(rgb, corners[6], corners[4], color=color)
	cv2.line(rgb, corners[4], corners[2], color=color)
	cv2.line(rgb, corners[2], corners[0], color=color)
	# 4 connecting lines
	cv2.line(rgb, corners[0], corners[1], color=color)
	cv2.line(rgb, corners[7], corners[6], color=color)
	cv2.line(rgb, corners[5], corners[4], color=color)
	cv2.line(rgb, corners[3], corners[2], color=color)
	# Final 2 connecting lines so we see the face of the bbox
	cv2.line(rgb, corners[1], corners[6], color=color)
	cv2.line(rgb, corners[0], corners[7], color=color)

	return rgb

def plotThisImage(rgb, labels, imageIndex):
	# Keep only the labels of this image index
	relevantKeys = ["bbox2d", "bbox2dCenter", "3dDimensions", "3dRotationY", "camera", "3dCenter"]
	thisLabelsIx = np.where(labels["originalRGBIndex"] == imageIndex)[0]
	labels = {k : labels[k][thisLabelsIx] for k in relevantKeys}

	centers, dims, alphas = labels["3dCenter"], labels["3dDimensions"], labels["3dRotationY"]
	BBoxes2D, BBoxes2DCenter = labels["bbox2d"], labels["bbox2dCenter"]
	camera = labels["camera"][0]
	numLabels = len(centers)

	for i in range(numLabels):
		center, dim, alpha = centers[i], dims[i], alphas[i]
		# rgb = draw2DBBox(rgb, BBoxes2D[i], BBoxes2DCenter[i], color=(1, 0, 0))
		rgb = draw3DBBox(rgb, center, dim, alpha, camera, color=(1, 0, 0))
	plt.imshow(rgb)
	plt.show()

def test_dataset(generator, numIters, valGenerator, valNumIters):
	for data, labels in generator:
		rgbs = labels["originalRGB"]
		numOrigImages = len(rgbs)

		for i in range(numOrigImages):
			plotThisImage(rgbs[i], labels, imageIndex=i)