import h5py
import numpy as np
from neural_wrappers.readers import DatasetReader
from neural_wrappers.utilities import resize_black_bars
import matplotlib.pyplot as plt

def getImageCrop(image, bbox2d):
	crop_image = image[bbox2d[1] : bbox2d[3], bbox2d[0] : bbox2d[2]]
	return resize_black_bars(crop_image, height=224, width=224, interpolation="bilinear")

class Reader(DatasetReader):
	def __init__(self, datasetPath):
		super().__init__(datasetPath, allDims=["rgb", "label", "calib"], \
			dataDims=["rgb"], labelDims=["label", "calib"], \
			normalizer = {
				"rgb" : ("min_max_normalize_rgb" , lambda x, dim : x / 255)
			}
		)

		self.dataset = h5py.File(self.datasetPath, "r")
		self.numData = {k : len(self.dataset[k]["rgb"]) for k in self.dataset}
		self.classes = ["Car", "Van", "Truck", "Pedestrian", "Person_sitting", "Cyclist", "Tram", "Misc", "DontCare"]

	# One image can have a variable amount of labels. We go through all of them that are vechicles and aren't very
	#  occluded or truncated. We store all cropped images, after resizing properly, in a new list and add them
	#  together with all other items of this batch.
	def processOneImage(self, rgb, labels):
		labels = labels.reshape(-1, 15)
		numLabels = len(labels)

		newRgbs = []
		newLabels = {"bbox2d" : [], "bbox2dCenter" : [], "3dDimensions" : [], "3dRotationY" : [], "3dCenter" : [], \
			"Z" : []}
		for i in range(numLabels):
			label = labels[i]
			Class, truncated, occluded, bbox2d, dimensions, center, rotationY = \
				int(label[0]), label[1], label[2], label[4 : 8], label[8 : 11], label[11 : 14], label[14]

			Class = self.classes[Class]
			if not Class in ["Car", "Truck", "Van", "Tram", "Pedestrian", "Cyclist"] or truncated >= 0.05:
				continue

			rotationY = (rotationY + 2 * np.pi) % (2 * np.pi)
			cropImage = getImageCrop(rgb, bbox2d.astype(np.int))
			bbox2dCenter = [(bbox2d[2] + bbox2d[0]) // 2, (bbox2d[3] + bbox2d[1]) // 2]

			newRgbs.append(cropImage)
			newLabels["bbox2d"].append(np.int32(bbox2d))
			newLabels["bbox2dCenter"].append(np.int32(bbox2dCenter))
			# height, width, length
			newLabels["3dDimensions"].append(np.float32(dimensions))
			newLabels["3dCenter"].append(np.float32(center))
			newLabels["3dRotationY"].append(np.float32(rotationY))
			newLabels["Z"].append(np.float32(center[-1]))
		return newRgbs, newLabels

	def processBatchImages(self, data, labels):
		rgbs = data["rgb"]
		cameras = labels["calib"]
		kittiLabels = labels["label"]
		MB = len(rgbs)

		newData, newLabels = [], {"bbox2d" : [], "bbox2dCenter" : [], "3dDimensions" : [], "3dRotationY" : [], \
			"camera" : [], "originalRGBIndex" : [], "3dCenter" : [], "Z" : []}
		for j in range(MB):
			data, labels = self.processOneImage(rgbs[j], kittiLabels[j])
			newLabels["camera"].extend([cameras[j]] * len(data))
			newLabels["bbox2d"].extend(labels["bbox2d"])
			newLabels["bbox2dCenter"].extend(labels["bbox2dCenter"])
			newLabels["3dRotationY"].extend(labels["3dRotationY"])
			newLabels["3dDimensions"].extend(labels["3dDimensions"])
			newLabels["3dCenter"].extend(labels["3dCenter"])
			newLabels["Z"].extend(labels["Z"])
			# This is stored so we know which crop belongs to which original image.
			newLabels["originalRGBIndex"].extend([j] * len(data))
			newData.extend(data)
		for k in newLabels:
			newLabels[k] = np.array(newLabels[k])
		newLabels["originalRGB"] = rgbs
		newData = np.array(newData, dtype=np.float32)
		return newData, newLabels

	def iterate_once(self, type, miniBatchSize):
		dataset = self.dataset[type]
		numIterations = self.getNumIterations(type, miniBatchSize, accountTransforms=False)

		for i in range(numIterations):
			startIndex = i * miniBatchSize
			endIndex = min((i + 1) * miniBatchSize, self.numData[type])
			assert startIndex < endIndex, "startIndex < endIndex. Got values: %d %d" % (startIndex, endIndex)
			numData = endIndex - startIndex

			for data, labels in self.getData(dataset, startIndex, endIndex):
				yield self.processBatchImages(data, labels)